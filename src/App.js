import React, { useState } from 'react';
import styled, {keyframes} from 'styled-components';
import myImg from './media/myPic.jpeg';

const Wrapper = styled.div`
  @import url('https://fonts.googleapis.com/css2?family=Ubuntu+Mono&display=swap');  
  font-family: 'Ubuntu Mono', monospace;
  height: 100vh;
  width: 100vw;
  background: red;
`

const TiltWarn = styled.div`
  @media (max-width: 600px) {
    position: fixed;
    height: 100vh;
    width: 100vw;
    z-index: 999999;
    background: black;
    top: 0;
    left: 0;
    display: flex;
    align-items: center;
    justify-content: center;
  }
  display: none;
`

const Welcome = styled.div`
  height: 100vh;
  background: #1A1C20;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const titleAnimation = keyframes`
  0% { color: #C06014; }
  40% { color: #FF8303; }
`

const Title = styled.h1`
  font-size: 9vmin;
  color: #C06014;
  margin-bottom: 5vh;
  text-shadow: .5vw .7vh 1vmax black;
  animation-name: ${titleAnimation};
  animation-duration: 1.6s;
  animation-iteration-count: infinite;
`

const Info = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
  width: 30vmax;
`

const Image = styled.img`
  width: 12vmax;
  border-radius: 2vw;
  border: .3vw solid white;
  box-shadow: 1vw 1.4vh 2vmax black;
`

const Text = styled.p`
width: 13vw;
color: #F3F4ED;
font-size: 1.5vmax;
text-shadow: .3vw .45vh .6vmax black;
`

const Spacer = styled.div`
background: #C06014;
height: 1vh;
width: 100vw;
`

const Projects = styled.div`
height: 100vh;
width: 100vw;
background-color: #1A1C20;
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
`

const Items = styled.div`
width: 90%;
display: flex;
justify-content: center;
margin-top: 7vh;
`

const Item = styled.div`
margin: 0 2.5vmax 0 2.5vmax;
background: #082032;
width: 15vw;
height: 15vw;
border-radius: 2vw;
border: .2vw solid black; 
box-shadow: .6vw 1vh 2vmax black;
transition: 2s;
display: flex;
flex-direction: column;
align-items: center;
justify-content: space-evenly;
font-size: 2vw;
color: white;
text-decoration: none;

&:hover {
  box-shadow: 1.2vw 1.8vh 2.4vmax black;
  border: .3vw solid #C06014; 
  transform: scale(1.15);
}
`

const ImageB = styled.img`
  width: 70%;
  border-radius: 2vw;
  border: .2vw solid white;
  box-shadow: .4vw .5vh 1vmax black;
`

const Career = styled.div`
  height: 100vh;
  width: 100vw;
  background-color: #1A1C20;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const Options = styled.div`
  width: 55vw;
  margin-top: 1vh;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
`

const Option = styled.button`
  background: none;
  border: none;
  color: #F3F4ED;
  font-size: 1.9vw;
  transition: .6s;

  &:hover {
    color: #C06014; 
    font-size: 2vw;
  }
`

const fadeIn = keyframes`
  0% { opacity: 0; }
  100% { opacity: 1; }
`

const BigInfo = styled.div`
  height: 40vh;
  width: 60vw;
  margin-top: 9vh;
  font-size: 1.9vw;
  color: #C06014;
  animation-name: ${fadeIn};
  animation-duration: 2s;
`

const Objectives = styled.div`
  height: 100vh;
  width: 100vw;
  background-color: #1A1C20;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const LinksButton = styled.button`
  width: 8vh;
  height: 8vh;
  margin: 1vh;
  border-radius: 3vh;
  position: fixed;
  z-index: 999;
  background: black;
  border: .3vh solid #C06014;
  color: #C06014;
  font-size: 6vh;
  transition: 1s;

  &:hover {
    transform: rotate(90deg);
  }
`

const Navbar = styled.div`
  width: 100vw;
  height: 10vh;
  position: fixed;
  background: black;
  transform: 2s;
  opacity: .6;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
`

const MoveLink = styled.a`
  text-decoration: none;
  color: white;
  font-size: 4vh;
  transition: 1s;

  &:hover {
    font-size: 4.5vh;
  }
`

const ScrollButton = styled.button`
  width: 10vh;
  height: 6vh;
  margin: 2vh;
  border-radius: 3vh;
  background: #C06014;
  border: .4vh solid black;
  color: black;
  font-size: 4vh;
  transition: 1s;
  box-shadow: .3vw .5vh 1vmax black;

  &:hover {
    transform: scale(1.1);
  }
`

const Footer = styled.div`
  height: 100vh;
  width: 100vw;
  background-color: #1A1C20;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const LinkWrap = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-top: 10vh;
  width: 60vw;
  justify-content: space-evenly;
`

const Linker = styled.a`
  text-decoration: none;
  color: white;
  font-size: 4vh;
  transition: 1s;

  &:hover {
    font-size: 4.5vh;
    color: #C06014;
  }
`

const projectList = [
{
    "title": "Antena Yagi",
    "img": "https://upload.wikimedia.org/wikipedia/commons/d/de/Yagifaltdipolp.jpg",
    "link": ""
  },
  {
    "title": "Tienda Online",
    "img": "https://resizer.glanacion.com/resizer/BuTvjBzeTwoEFbwcn1mRuhu58Ak=/768x0/filters:quality(80)/cloudfront-us-east-1.images.arcpublishing.com/lanacionar/TYKUQAULKZCDZIFN2WCD2XXI3E.jpg",
    "link": ""
  },
  {
    "title": "Eurorack",
    "img": "https://files.soniccdn.com/files/2019/12/12/portada.png",
    "link": ""
  },
  {
    "title": "VST Plugins",
    "img": "https://www.homestudioproductions.com/wp-content/uploads/2020/11/xerum-xfer.jpg",
    "link": ""
  }
]

function App() {
  const [display, setDisplay] = useState('');
  const [showNavbar, setShowNavbar] = useState(false);
  const [scroll, setScroll] = useState(0);
  const objectives = ['DevOps','Full Stack Developer','Electronica Analogica [Audio, Comunicaciones, etc]', 'Node', 'AWS', 'Mongo', 'Express', 'MySQL', 'Docker']

  return (
    <Wrapper>
      <TiltWarn>
        <Title>Gire su celular</Title>
      </TiltWarn>
      <LinksButton onClick={()=>{setShowNavbar(!showNavbar)}}>=</LinksButton>
      {showNavbar ? (
      <Navbar>
        <MoveLink href='#bienvenida'>Bienvenida</MoveLink>
        <MoveLink href='#proyectos'>Proyectos</MoveLink>
        <MoveLink href='#estudios'>Estudios</MoveLink>
        <MoveLink href='#objetivos'>Objetivos</MoveLink>
        <MoveLink href='#footer'>Contacto</MoveLink>
      </Navbar>
      ) : ''}
      <Welcome id='bienvenida'>
        <Title>Manuel Ahumada</Title>
        <Info>
          <Image src={myImg}/>
          <Text>Tecnico electronico especializado en DevOps y WebDev</Text>
        </Info>
      </Welcome>
      <Spacer/>
      <Projects id="proyectos">
        <Title>Mis proyectos</Title>
        <Items>
          {projectList.map((project)=>{
            const {title, img, link} = project;
            return (
              <Item>
                <a href={link} target="_blank" style={{color: 'white', textDecoration: 'none'}} rel='noreferrer'>{title}</a>
                <ImageB src={img} /> 
              </Item>
            )
          })}
        </Items>
      </Projects>
      <Spacer/>
      <Career id="estudios">
        <Title onClick={()=>{setDisplay('')}}>Estudios</Title>
        <Options>
          <Option onClick={()=>{setDisplay('Academico')}}>Academico</Option>
          <Option onClick={()=>{setDisplay('Digital')}}>Digital</Option>
          <Option onClick={()=>{setDisplay('Artistico')}}>Artistico</Option>
        </Options>
        {display === 'Academico' ? (
          <BigInfo>
            <ul>
              <li>Tecnicatura en electrónica en el Instituto La Salle Florida</li>  
              <li>CURSANDO Ingeniería Electrónica en la Facultad de Ingeniería, Universidad de Buenos Aires (FIUBA) 1er año</li> 
              <li>Lenguaje inglés a nivel avanzado fluido y lengua japonesa certificado N5 aprobado</li>
              <li>Pasantia ejercito nacional sistema de antena Yagi motorizada con parametros automaticamente transmitidos a la misma mediante GPredict y muestra de datos en aplicacion web</li>
            </ul>  
          </BigInfo>
        ) : (display === 'Digital') ? (
          <BigInfo>
            <ul>
              <li>Bash</li>
              <li>HTML5</li>
              <li>CSS</li>
              <li>JavaScript (JS)</li>
              <li>React.js Framework (Styled-components, react-router, hooks)</li>
              <li>C / C++</li>
              <li>Arduino / Raspberry PI</li>
              <li>Python 3</li>
              <li>APIs</li>
              <li>GNU/Linux + i3wm</li>
              <li>GIT</li>
              <li>Servidores / Networking (SSH, VPN, APACHE, etc)</li>
            </ul>
          </BigInfo>
        ) : (display === 'Artistico') ? (
          <BigInfo>
            <ul>
              <li>Produccion musical freelance [Hip-Hop, Trap, EDM, Ambiental, etc] desde 2018</li>
              <li>Lanzamientos de musica electronica como productor solista</li>
              <li>Ensenanza de produccion y teoria musical online en lengua INGLES desde 2019</li>
              <li>Proyecto personal de sintetizador 100% analógico modular con placas caseras, con la presencia de módulos: VCO, VCA, ADSR, Filtro HP/LP, Mixer mono/estéreo, Multiple, Fuente de poder</li>
            </ul>
          </BigInfo>
        ) : <div style={{height: '40vh', marginTop: '9vh'}}></div>}
      </Career>
      <Spacer/>
      <Objectives id="objetivos">
          <Title>Objetivos</Title>
          <div style={{display: 'flex', alignItems: 'center', height: '30vh', justifyContent: 'space-between', width: '40vw'}}>
            <ScrollButton onClick={()=>{scroll>0 ? setScroll(scroll-1) : setScroll(scroll)}}>&#60;</ScrollButton>
            <BigInfo style={{margin: '0', height: 'auto', textAlign: 'center'}}>{objectives[scroll]}</BigInfo>
            <ScrollButton onClick={()=>{scroll<objectives.length-1 ? setScroll(scroll+1) : setScroll(scroll)}}>&#62;</ScrollButton>
          </div>
      </Objectives>
      <Spacer/>
      <Footer id='footer'>
        <Title>Contacto</Title>
        <LinkWrap>
          <Linker href='https://api.whatsapp.com/send?phone=541159266209&text=Hola%20Manuel!%20Vengo%20de%20tu%20pagina%20web...' target='_blank'>Whatsapp</Linker>
          <Linker href='https://gitlab.com/ahumadamanuel' target='_blank'>Gitlab</Linker>
          <Linker href='https://www.linkedin.com/in/manuel-ahumada-85b150219/' target='_blank'>LinkedIn</Linker>
          <Linker href='https://www.instagram.com/manuahumada4/' target='_blank'>Instagram</Linker>
        </LinkWrap>
      </Footer>
    </Wrapper>
  )
}

export default App
